<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
  Allgemeine Attribute sind
  runtimeChangeable=true/false  bedeutet zur Laufzeit äerbar
  comment=""
-->
<properties>
<!--		
		auto-logout="30"
-->
	<server 
		runtimeChangeable="false" 
		systemtype="ziel" 
		compression="true" 
		password-length="6" 
		auto-logout-named="30"
		auto-logout-concurrent="5"
		enable-ssl="false" 
		x-serveraddress="localhost:8080"
		email-serveradress="http://localhost:8080/psap"
		pdf-serveraddress=""
		comment="Den Server im test-Modus starten und die GZIP-Compression-Einschalten. pdf-serveraddress ist nur notwendig, wenn serveraddress nicht angegeben ist" 
	/>
	
	<TimeZone id="Europe/Berlin"/>

	<UploadSize runtimeChangeable="false" comment="Angabe in Byte. Es muessen etwa 3kB jeweils als overhead beruecksichtigt werden">
		<Max comment="Insgesamt">4M</Max>
		<ImageSize comment="Bilder">20K</ImageSize>
	</UploadSize>

	<pbeans runtimeChangeable="false" comment="Nutze die DB-Instance mit der id=test.sapdb" use-db="oracle">
		<dbInstances init="<t:minPoolSize/>" max="<t:maxPoolSize/>" comment="initiale Groesse des Pools, entsprechend auch initiale/maximale Anzahl an DB Connections" >
			<db id="mssql" url="jdbc:sqlserver://<t:jdbcDescriptor/>" driver="com.microsoft.sqlserver.jdbc.SQLServerDriver" username="<t:dbuser/>" password="<t:dbpass/>" comment="" />
		</dbInstances>
	</pbeans>

	<emails runtimeChangeable="true" comment="" >
		<Administrator comment="Email fuer Admin-User"><t:Admin_email/></Administrator>
		<Support comment="Support-EMail-adresse">blueant-support@proventis.net</Support>
		<SupportCC comment="Support-CC-EMail-adresse">blueant-support-cc@proventis.net</SupportCC>
	</emails>

	<ShowModules class="net.proventis.base.servlet.servletproperty.ShowModuleTagMapper" runtimeChangeable="false" comment="Aktivierung Benutzerspezifischer Module, Property wird nur generiert wenn value auf true gesetzt, Property-Name: ShowModules.ServiceRequests" >
		<ServiceRequests comment="Wartungsauftraege">false</ServiceRequests>
		<OrderProcessing comment="Auftragsbearbeitung">false</OrderProcessing>
		<OtherModule comment="anderes Benutzermodul">false</OtherModule>
	</ShowModules>
	<jobs priority="5" comment="Prioritäder Jobs" />

	<FilterConfig>
		<WebServiceLegacyFilter>
			<servicePath>/services/</servicePath>
			<resourcePath>/WEB-INF/webserviceLegacy/</resourcePath>
			<!-- versionCompatibility>8.2.1</versionCompatibility -->
		</WebServiceLegacyFilter>

		<BlueAntSSOFilter>
			<!-- 
				Logging
				 0: Nothing
				 1: Critical [default]
				 2: Basic info. (Can be logged under load) 
				 3: Detailed info. (Highest recommended level for production use) 
				 4: Individual smb messages
				 6: Hex dumps
			 -->
			 <jcifs.util.loglevel>1</jcifs.util.loglevel>

			<!-- Schaltet NTMLv2 ab, was scheinbar nicht von allen Browsern (FF 3.5) unterstüird -->
			<jcifs.smb.client.useExtendedSecurity>false</jcifs.smb.client.useExtendedSecurity>
			<jcifs.smb.lmCompatibility>2</jcifs.smb.lmCompatibility>
			<ntlmauth.enableNTLMforFirefox>true</ntlmauth.enableNTLMforFirefox>
			
			<!-- UnterdrüSO-Authentifizierung fütimmte Mimetypes (z.B. MSProject). Der param-value kann durch weitere, kommaseparierte Mimetypes erweitert werden. -->
			<skip-mime-types>x-application/psap-msproject</skip-mime-types>
	
	
			<!-- 
				1. Variante: Den PDC üINS suchen
				Schüor PDC-Ausfall bzw. Äderungen an der IP vom PDC
			 -->
			<!-- jcifs.netbios.wins>192.168.x.y</jcifs.netbios.wins -->
			
			<!-- 
				2. Variante: PDC-IP direkt angeben
				ACHTUNG: Fät der PDC aus oder äert sich die IP, dann funktioniert BA nicht mehr!!!
			 -->
			<jcifs.http.domainController>192.168.x.y</jcifs.http.domainController>
			
			<!-- Name der Domä definieren und Credentials fü Computerkonto von BA -->
			<jcifs.smb.client.domain>Domä</jcifs.smb.client.domain>
			<jcifs.smb.client.username>Computerkonto</jcifs.smb.client.username>
			<jcifs.smb.client.password>Passwort</jcifs.smb.client.password>
		
		</BlueAntSSOFilter>
		
		<ProjectplaceSSO>
			<secret1>SECRET_1</secret1>
			<secret2>SECRET_2</secret2>
			<host></host>
		</ProjectplaceSSO>

	</FilterConfig>

</properties>

