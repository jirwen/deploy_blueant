<Server port="<t:tcadminport/>" shutdown="SHUTDOWN" debug="0">

  <!-- Define an Apache-Connector Service -->
  <Service name="dbmserver">

    <Connector
        port="<t:tcajpport/>"
        maxThreads="500" minSpareThreads="5" maxSpareThreads="50"
        enableLookups="false" redirectPort="8443"
        acceptCount="10" debug="3" connectionTimeout="20000"
        disableUploadTimeout="false"
        useURIValidationHack="false"
        secure="true"
        protocol="AJP/1.3"/>

    <Engine
            jvmRoute=""
            name="<t:urlhost/>"
            defaultHost="<t:urlhost/>"
            debug="3">

      <!--
      <Valve className="org.apache.catalina.valves.RequestDumperValve"/>
      -->

      <!-- Because this Realm is here, an instance will be shared globally -->
      <Realm className="org.apache.catalina.realm.MemoryRealm" />

              <Host name="<t:urlhost/>" debug="0" appBase="webapps"
              unpackWARs="true"
              autoDeploy="false"
              xmlValidation="false" xmlNamespaceAware="false">


         <Context path="/<t:context/>" docBase="<t:context/>" debug="3" cookies="true" reloadable="true" useHttpOnly="true">

            <Resource name="jdbc/<t:PoolName/>DataSource"
                       auth="Container"
                        factory="oracle.ucp.jdbc.PoolDataSourceImpl"
                        type="oracle.ucp.jdbc.PoolDataSource"
                        connectionFactoryClassName="oracle.jdbc.pool.OracleDataSource"
                        minPoolSize="<t:minPoolSize/>"
                        maxPoolSize="<t:maxPoolSize/>"
                        inactiveConnectionTimeout="20"
                        user="<t:dbuser/>"
                        password="<t:dbpass/>"
                        url="jdbc:oracle:thin:@<t:jdbcDescriptor/>"
                        connectionPoolName="<t:PoolName/>Pool"
                        validateConnectionOnBorrow="true"
                        sqlForValidateConnection="select 1 from DUAL" />


        </Context>
      </Host>
    </Engine>
  </Service>
</Server>


