set :application_name,         "blueant"
set :module_name,              "blueant"


set :cleanup_workspace_before, false

set :log_level,                "INFO"
set :log_file,                 "deploy.log"
set :log_file_level,           "DEBUG"

Deployment.deliver do
  $log.writer.info "Starting ..."

  # get latest configuration for this app from cdb
  $log.writer.info "Get latest parameters from cdb"
  config = get_all_config_parameters

  $log.writer.info "Create tomcat base directorys"

  mkdir("target/tomcat/conf")
  mkdir("target/tomcat/#{config[:module_name].downcase}_#{config[:env].downcase}/webapps/blueant")


  # copy additional files

  $log.writer.info "Copy additional files to tomcat directory"
  
  $log.writer.info "Get application from nexus maven SinnerSchrader repository"
  get_file_from_maven( \
    "nexus/content/repositories/#{config[:module_name].downcase}/com/sinnerschrader/#{config[:module_name].downcase}/#{config[:application_name].downcase}/#{@version['app']}/#{config[:application_name].downcase}-#{@version['app']}.war", "source/" )
  $log.writer.info "Unzip application"
  $log.writer.info "source/#{config[:application_name].downcase}-#{@version['app']}.war"
  tomcat_target = "target/tomcat/#{config[:module_name].downcase}_#{config[:env].downcase}/webapps"
  unzip_file("source/#{config[:application_name].downcase}-#{@version['app']}.war", tomcat_target)

  # parse template
  $log.writer.info "parse template"
  web_inf = "#{tomcat_target}/#{config[:module_name].downcase}/WEB-INF"
  $log.writer.info "#{web_inf}/properties/"
  parse_template('config/WEB-INF/properties/', "#{web_inf}/properties/", config)
  patch_properties("config/WEB-INF/properties/log4j.properties", "#{web_inf}/properties/", :assigner => '=', :patch_set => 'log4j_properties')

  # upload to tomcat
  #worker.rsync("target/#{application_name}/", "#{catalina_home}/webapps/#{tomcat_context}", :remote_host => server)
  # $log.writer.info "Deploy to tomcat"
  # tomcat_deploy(:runners => [hostname])

  # $log.writer.info "Send mail to deployment team #{config['deploy_email_to']}"
  # report_by_mail

  $log.writer.info "Deployment done!"
end
